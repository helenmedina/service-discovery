#include "TextServerConnection.h"
#include "Poco/Net/TCPServerParams.h"
#include "Poco/Net/StreamSocket.h"
#include "Poco/Net/ServerSocket.h"
#include "Poco/Timestamp.h"
#include "Poco/DateTimeFormatter.h"
#include "Poco/DateTimeFormat.h"
#include "Poco/Exception.h"
#include "Poco/Util/ServerApplication.h"
#include "Poco/Util/Application.h"
#include "Poco/Util/Option.h"
#include "Poco/Util/OptionSet.h"
#include "Poco/Util/HelpFormatter.h"
#include "Poco/StreamCopier.h"
#include "Poco/Timespan.h"
#include "Poco/FileStream.h"
#include "Poco/Net/SocketStream.h"
#include <ostream>
#include <iostream>
#include <sstream>
#include <fstream>

#include "Poco/Crypto/Cipher.h"
#include "Poco/Crypto/CipherFactory.h"
#include "Poco/Crypto/CipherKey.h"
#include "Poco/Crypto/CryptoStream.h"
#include "Poco/Exception.h"
#include "Poco/Crypto/CryptoException.h"

using Poco::Net::ServerSocket;
using Poco::Net::StreamSocket;
using Poco::Net::TCPServerConnection;
using Poco::Net::TCPServerConnectionFactory;
using Poco::Net::TCPServer;
using Poco::Timestamp;
using Poco::DateTimeFormatter;
using Poco::DateTimeFormat;
using Poco::Util::ServerApplication;
using Poco::Util::Application;
using Poco::Util::Option;
using Poco::Util::OptionSet;
using Poco::Util::HelpFormatter;



TextServerConnection::TextServerConnection(const StreamSocket& s, const std::string& format):TCPServerConnection(s),_format(format)
{
	bSendOperation = false;
	bReceiveOperation = false;
	parseConfigFile();
}
	
void TextServerConnection::run()
{
	Application& app = Application::instance();
	std::cout << "Text server running" << std::endl;
	
	try{	
		//starts listening
		app.logger().information("Request from " + this->socket().peerAddress().toString());
		
		Poco::Net::SocketStream istr(this->socket());
		//copy stream received in socket
		std::ostringstream os;		
		Poco::StreamCopier::copyStream(istr, os);	
		std::string dataStream = os.str();
		//parse request received
		getParameters(dataStream);
		//build outputfile
		std::string outputFile = userNameText + "_" + fileNameText;
		std::cout << "OutputFile: " <<  outputFile << std::endl;
		
		if (bSendOperation)
		{
			std::cout << "............sending operation ............." << std::endl;
			
			Poco::FileOutputStream ostr(outputFile, std::ios::out);
			ostr << dataStream;
		}
		else 
		{
			std::cout << "............receiving operation ............" << std::endl;
			
			Poco::FileInputStream streamToSend(outputFile, std::ios::binary);
			Poco::StreamCopier::copyStream(streamToSend, istr);
			istr.flush();
		}
		
	}
	catch (Poco::Exception& exc)
	{
			app.logger().log(exc);
	}
	
	//reset the type operation	
	resetOperation();
}
	
void TextServerConnection::resetOperation()
{
	bSendOperation = false;
	bReceiveOperation = false;
}

void TextServerConnection::getParameters(std::string &dataStream)
{
	
	//find peration type
	getParam(operationSend, dataStream);
	getParam(operationReceive, dataStream);
	//find user
	getParam(typeUserCommand, dataStream);		
	//find name
	getParam(typeFileCommand, dataStream);
	
}

void TextServerConnection::getParam(std::string command, std::string &dataStream)
{
	std::string::size_type i = -1;
	i = dataStream.find(command);

	if (i != std::string::npos)
	{
		if (command == operationSend)
		{
			bSendOperation = true;		
			dataStream.erase(i, command.length());
		}
		else if (command == operationReceive)
		{
			bReceiveOperation = true;		
			dataStream.erase(i, command.length());
		}
		else if (command == typeUserCommand)
		{
			std::string::size_type posUserID = i + command.length();
			userNameText = dataStream.substr (posUserID, stoi(sizeUser));
			dataStream.erase(i, command.length() + stoi(sizeUser));
		}
		else if (command == typeFileCommand)
		{
			std::string::size_type posFile = i + command.length();
			fileNameText = dataStream.substr (posFile, stoi(sizeFile));
			dataStream.erase(i, command.length() + stoi(sizeFile));
		}
		else
			std::cout << "Command unknonw " << std::endl;
	}	

}

void TextServerConnection::parseConfigFile()
{
	std::ifstream configFile("config.cfg");
	std::string line;
	std::istringstream sin;

	//read config file
	while (std::getline(configFile, line)) 
	{
		 sin.str(line.substr(line.find("=") + 1));
		if (line.find("operationSend") != std::string::npos) 
		{			
			operationSend = sin.str();
		}
		else if (line.find("operationReceive") != std::string::npos) {
			operationReceive = sin.str();
		}
		else if (line.find("typeUserCommand") != std::string::npos) {
			typeUserCommand = sin.str();
		}
		else if (line.find("sizeUser") != std::string::npos) {
			sizeUser = sin.str();
		}
		else if (line.find("typeFileCommand") != std::string::npos) {
			typeFileCommand = sin.str();
		}
		else if (line.find("sizeFile") != std::string::npos) {
			sizeFile = sin.str();
		}
	 }
	 
	 sin.clear();
	
}
