#include "Poco/Net/TCPServer.h"
#include "Poco/Net/TCPServerConnection.h"
#include "Poco/Net/TCPServerConnectionFactory.h"
#include "TextServerConnection.h"

#ifndef TEXT_SERVER_CONNECTION_FACTORY_H
#define TEXT_SERVER_CONNECTION_FACTORY_H

class TextServerConnectionFactory: public Poco::Net::TCPServerConnectionFactory
{
public:
	TextServerConnectionFactory(const std::string& format):
		_format(format)
	{
	}
	
	Poco::Net::TCPServerConnection* createConnection(const Poco::Net::StreamSocket& socket)
	{
		return new TextServerConnection(socket, _format);
	}

private:
	std::string _format;
};

#endif //TEXT_SERVER_CONNECTION_FACTORY_H
