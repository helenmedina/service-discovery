
#ifndef TEXT_SERVER_CONNECTION_H
#define TEXT_SERVER_CONNECTION_H

#include "Poco/Net/TCPServer.h"
#include "Poco/Net/TCPServerConnection.h"

class TextServerConnection: public Poco::Net::TCPServerConnection
{
public:
	TextServerConnection(const Poco::Net::StreamSocket& s, const std::string& format);
	void run();

private:
	void getParameters(std::string &dataStream);
	void parseConfigFile();
	void resetOperation();
	void getParam(std::string command, std::string &dataStream);
	
	//members
	std::string operationSend;
	std::string operationReceive;
	std::string typeUserCommand;
	std::string sizeUser;
	std::string typeFileCommand;
	std::string sizeFile;
	std::string _format;
	std::string userNameText;
	std::string fileNameText;
	bool bSendOperation;
	bool bReceiveOperation;

};

#endif //TEXT_SERVER_CONNECTION_H
