#include "ImageServerConnection.h"
#include "ImageServerConnectionFactory.h"
#include "Poco/Net/TCPServerParams.h"
#include "Poco/Net/StreamSocket.h"
#include "Poco/Net/ServerSocket.h"
#include "Poco/Net/SocketStream.h"
#include "Poco/Exception.h"
#include "Poco/Util/ServerApplication.h"
#include "Poco/Util/Application.h"
#include "Poco/Util/Option.h"
#include "Poco/Util/OptionSet.h"
#include "Poco/Util/HelpFormatter.h"
#include "Poco/Timestamp.h"
#include "Poco/DateTimeFormatter.h"
#include "Poco/DateTimeFormat.h"
#include <iostream>
#include <fstream>
#include <sstream>

using Poco::Util::ServerApplication;
using Poco::Util::Application;
using Poco::Util::Option;
using Poco::Util::OptionSet;
using Poco::Util::HelpFormatter;


class ImageStoreServer: public Poco::Util::ServerApplication
{
public:
	ImageStoreServer(): _helpRequested(false)
	{
		//listen in port 9911
		port = 9911;
		parsePortRegister();
	}
	
	~ImageStoreServer()
	{
		unregister();
	}

protected:
	
	void parsePortRegister()
	{
		//read config file
		std::ifstream configFile("config.cfg");
		std::string line;
		std::istringstream sin;

		while (std::getline(configFile, line)) 
		{
			 sin.str(line.substr(line.find("=")+1));
			if (line.find("registerPort") != std::string::npos) 
			{
			  std::cout<<"registerPort "<<sin.str()<<std::endl;
			  sin >> portRegister;
			}
		 }
		 sin.clear();
			
	}
	
	void unregister()
	{
		//opens a socket in localhost
		Poco::Net::SocketAddress sa("127.0.0.1", portRegister);
		//tcp stream socket
		Poco::Net::StreamSocket ss1(sa);
		//sends the request unregister to registry server
		char const *buffer = "type:unregister service:ImgStore ";
		ss1.sendBytes(buffer, 1000);
		std::cout << "service unregistered." << buffer << std::endl;
	}

	void registerServer()
	{
		//opens a socket in localhost
		Poco::Net::SocketAddress sa("127.0.0.1", portRegister);
		//tcp stream socket
		Poco::Net::StreamSocket ss1(sa);
		//sends the request register to registry server
		char const *buffer = "type:register service:ImgStore port:9911 ip:127.0.0.1 ";
		ss1.sendBytes(buffer, 1000);
		std::cout << "service registered." << buffer << std::endl;
	}
	void initialize(Application& self)
	{
		loadConfiguration(); // load default configuration files, if present
		ServerApplication::initialize(self);
	}
		
	void uninitialize()
	{
		ServerApplication::uninitialize();
	}

	void defineOptions(OptionSet& options)
	{
		ServerApplication::defineOptions(options);
		
		options.addOption(
			Option("help", "h", "display help information on command line arguments")
				.required(false)
				.repeatable(false));
	}

	void handleOption(const std::string& name, const std::string& value)
	{
		ServerApplication::handleOption(name, value);

		if (name == "help")
			_helpRequested = true;
	}

	void displayHelp()
	{
		HelpFormatter helpFormatter(options());
		helpFormatter.setCommand(commandName());
		helpFormatter.setUsage("OPTIONS");
		helpFormatter.setHeader("A server application that stores image files.");
		helpFormatter.format(std::cout);
	}

int main(const std::vector<std::string>& args)
{
	if (_helpRequested)
	{
		displayHelp();
	}
	else
	{
		
		// set-up a server socket
		Poco::Net::ServerSocket svs(port);
		std::string format(config().getString("TimeServer.format", Poco::DateTimeFormat::ISO8601_FORMAT));
		// set-up a TCPServer instance
		Poco::Net::TCPServer imageServer(new ImageServerConnectionFactory(format), svs);
		
		try
		{
			registerServer();
		}
		catch(...)
		{
			std::cout << "service not registered or rejected." << std::endl;
			return Application::EXIT_UNAVAILABLE;
		}
				
		// start the TCPServer
		imageServer.start();
		// wait for CTRL-C or kill
		waitForTerminationRequest();
		// Stop the TCPServer
		imageServer.stop();
	}
	return Application::EXIT_OK;
}
	
private:

	bool _helpRequested;
	Poco::UInt16 port;	
	Poco::UInt16 portRegister;
	
};


int main(int argc, char** argv)
{
	ImageStoreServer app;
	return app.run(argc, argv);
}


