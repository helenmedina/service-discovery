#include "Poco/Net/TCPServer.h"
#include "Poco/Net/TCPServerConnection.h"
#ifndef IMAGE_SERVER_CONNECTION_H
#define IMAGE_SERVER_CONNECTION_H

class ImageServerConnection: public Poco::Net::TCPServerConnection
{
public:
	ImageServerConnection(const Poco::Net::StreamSocket& s, const std::string& format);
	void run();

private:
	void getParameters(std::string &dataStream);
	void parseConfigFile();
	void resetOperation();
	void getParam(std::string command, std::string &dataStream);
	std::string operationSend;
	std::string operationReceive;
	std::string typeUserCommand;
	std::string sizeUser;
	std::string typeFileCommand;
	std::string sizeFile;
	std::string _format;
	std::string userNameText;
	std::string fileNameText;
	bool bSendOperation;
	bool bReceiveOperation;

};

#endif //IMAGE_SERVER_CONNECTION_H
