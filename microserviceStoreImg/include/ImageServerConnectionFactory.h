#include "Poco/Net/TCPServer.h"
#include "Poco/Net/TCPServerConnection.h"
#include "Poco/Net/TCPServerConnectionFactory.h"
#include "ImageServerConnection.h"

#ifndef IMAGE_SERVER_CONNECTION_FACTORY_H
#define IMAGE_SERVER_CONNECTION_FACTORY_H

class ImageServerConnectionFactory: public Poco::Net::TCPServerConnectionFactory
{
public:
	ImageServerConnectionFactory(const std::string& format):
		_format(format)
	{
	}
	
	Poco::Net::TCPServerConnection* createConnection(const Poco::Net::StreamSocket& socket)
	{
		return new ImageServerConnection(socket, _format);
	}

private:
	std::string _format;
};

#endif //IMAGE_SERVER_CONNECTION_FACTORY_H
