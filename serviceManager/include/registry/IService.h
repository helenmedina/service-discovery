#ifndef ISERVICE_H
#define ISERVICE_H

class IService
{
	
public:
	
	IService(const char* port = (char*)"8080", const char* ip = (char*)"localhost");

	const char* getPort() const;
	const char* getIP() const;
	void setPort(const char*);
	void setIP(const char*);
	virtual ~IService() = 0;

private:
	char* m_port;
	char* m_ip;
};

#endif //ISERVICE_H
