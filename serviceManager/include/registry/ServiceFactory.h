#ifndef SERVICE_FACTORY_H
#define SERVICE_FACTORY_H

#include "IFactory.h"

class IService;

class ServiceFactory : public IFactory
{
public:
	std::unique_ptr<IService> createService(const char *type);
};

#endif //SERVICE_FACTORY_H
