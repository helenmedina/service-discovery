#ifndef TEXT_DATA_SERVICE_H
#define TEXT_DATA_SERVICE_H
#include "IService.h"

class TextDataService : public IService {
	
public:
	TextDataService(const char* port, const char* ip);
	TextDataService();
	virtual ~TextDataService()noexcept;
	
};

#endif //TEXT_DATA_SERVICE_H
