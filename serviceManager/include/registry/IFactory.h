#ifndef IFACTORY_H
#define IFACTORY_H

#include "IService.h"
#include <memory>

class IFactory
{
	virtual std::unique_ptr<IService> createService(const char *type) = 0;
};

#endif //IFACTORY_H
