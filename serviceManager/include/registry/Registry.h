#ifndef REGISTRY_H
#define REGISTRY_H
#include <map>
#include <memory>
#include "Poco/Thread.h"
#include "Poco/Runnable.h"
#include "IService.h"
#include "ServiceFactory.h"

//defines the services type
enum class eServicesType
{
	None = 0,
	TextService,
	ImgService
};

class Registry : public Poco::Runnable
{
	
public:
	Registry();
	virtual void run();
	void registerService (std::shared_ptr<IService> service, eServicesType serviceID);
	void unregisterService(eServicesType serviceID);
	std::map<eServicesType, std::shared_ptr<IService>>::iterator findService(eServicesType serviceID);
	std::shared_ptr<IService> getService (eServicesType serviceID);
	bool stop;
	
	
private:
	 std::map<eServicesType, std::shared_ptr<IService>> servicesRegistered;
	 void HandleRequest(std::string &msg);
	 void parseMsg(std::map<std::string, std::string>& buffer, std::string delimiter, std::string msg);
	 std::string findElement(const std::map<std::string, std::string> &buffer, std::string element);
	 eServicesType getServiceType(std::string strService);
	 void buildServiceAndRegister(std::map<std::string, std::string>& buffer);
	 void removeService(const std::map<std::string, std::string>& buffer);
	 
	 const int REGISTRY_PORT = 9912;
	 const int BUFFER_SIZE = 100000;
};

	

#endif //REGISTRY_H
