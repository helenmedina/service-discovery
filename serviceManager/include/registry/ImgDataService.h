#ifndef IMG_DATA_SERVICE_H
#define IMG_DATA_SERVICE_H
#include "IService.h"

class ImgDataService : public IService
{
	
public:
	ImgDataService(const char* port, const char* ip);
	ImgDataService();
	virtual ~ImgDataService()noexcept;
	
};

#endif //IMG_DATA_SERVICE_H
