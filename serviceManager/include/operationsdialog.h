#ifndef OPERATIONSDIALOG_H
#define OPERATIONSDIALOG_H

#include <QDialog>

class QHBoxLayout;
class QPushButton;
class QCheckBox;
class QLineEdit;
class ServiceManager;

class OperationsDialog : public QDialog
{
    Q_OBJECT
public:
    OperationsDialog(ServiceManager*);
    QString getPathFile() const;
    QString getFileNameToReceive() const;
    
signals:
	void sendImageFile();
	void sendTextFile();
	void receiveImageFile();
	void receiveTextFile();
	void showServiceManager();
	
private slots:
    void enableReceiveButton();
    void enableBrowseButton();
    void handleOperationBrowse();
    void handleSendButton();
    void handleOperationReceive();
    void handleOperationDirBrowse();
    void showMainWindow();

private:

   QPushButton *browseFile;
   QPushButton *browseDirectory;
   QPushButton *sendTextFileButton;
   QPushButton *receiveFileButton;
   QCheckBox *checkBoxText;
   QCheckBox *checkBoxImg;
   QCheckBox *checkBoxTextReceive;
   QCheckBox *checkBoxImgReceive;
   QLineEdit *edFileName;
   ServiceManager *serviceManager;
   QString pathFile;
   QString fileNameToReceive;
};

#endif // OPERATIONSDIALOG_H
