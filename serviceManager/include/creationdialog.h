#ifndef CREATIONDIALOG_H
#define CREATIONDIALOG_H
#include <QDialog>
class QLabel;
class QLineEdit;
class QPushButton;

class CreationDialog : public QDialog
{
    Q_OBJECT
    
public:
    CreationDialog();
    QString getName() const;
    QString getUserName() const;
    QString getPassword() const;
    QString getCode() const;

signals:
    void sendDataToController();
    
public slots:
    void saveData();
    
private:
    void initElements();
    bool validatePassword(QString password);
    
    QLabel *lbName;
    QLineEdit *edName;
    QLineEdit *edUserName;
    QLabel *lbPassword;
    QLineEdit *edPassword;
    QLabel *lbConfirmationPassword;
    QLineEdit *edConfirmationPassword;
    QLabel *lbCode;
    QLabel *lbUserName;
    QLineEdit *edCode;
    QPushButton *btnOk;
    QPushButton *btnCancel;

    QString name;
    QString userName;
    QString password;
    QString code;
    QString confirmationPassword;
};

#endif // CREATIONDIALOG_H
