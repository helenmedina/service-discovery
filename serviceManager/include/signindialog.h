#ifndef SIGNINDIALOG_H
#define SIGNINDIALOG_H
#include <QDialog>

class QLineEdit;

class SignInDialog : public QDialog
{
    Q_OBJECT
public:
    SignInDialog();
    QString getUsername() const;
    QString getPassword() const;

signals:
    void checkCredentialsInController();
    
public slots:
    void checkCredentials();
    
private:
     QLineEdit *edUserName;
     QLineEdit *edPassword;
};
#endif // SIGNINDIALOG_H
