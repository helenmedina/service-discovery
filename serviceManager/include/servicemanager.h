#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <map>
#include "exceptionGeneral.h"

class CreationDialog;
class SignInDialog;
class OperationsDialog;
class ModuleController;


namespace Ui {
class MainWindow;
}

class ServiceManager : public QMainWindow
{
    Q_OBJECT

public:
    explicit ServiceManager(QWidget *parent = 0);
    ~ServiceManager();
     QString getFileName(QString &pathFile) const;
     

signals:
    void closeSignInDialog();
    void closeCreationDialog();

public slots:
    void SignInService();
    void sendDataToController();
    void sendImageFile();
	void sendTextFile();
	void receiveImageFile();
	void receiveTextFile();
	void showServiceManager();

private slots:
    void on_createAccountButton_clicked();
    void on_signButton_clicked();


private:
	void setFileIdentifier(std::string file);
	bool setUserIdentifierValid(QString user);
	std::string getFileFromList(std::string file);
	std::string getUserFromList(std::string user);

	void initRegister();
    Ui::MainWindow *ui;
    CreationDialog *creationDialog;
    SignInDialog *signDialog;
    OperationsDialog *operations;
    //stores all users
    std::map<std::string, std::string> userList;
    //store  all files
    std::map<std::string, std::string> fileList;
    //username identifier
    long int userIdentifier;
    //file identifier
    long long int fileIdentifier;
    
    //max value for username 
    static const long int USER_IDENTIFIER_MAX = 999999;
    //max value for filename
	static const long long int FILE_IDENTIFIER_MAX = 9999999999;
	//max size for username
	static const int USER_MAX_SIZE = 6;
	//max size for filename
	static const int FILE_MAX_SIZE = 10;
};

#endif // MAINWINDOW_H
