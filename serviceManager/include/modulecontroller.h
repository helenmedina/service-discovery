#ifndef MODULE_CONTROLLER_H
#define MODULE_CONTROLLER_H
#include "authorizationhandler.h"
#include "Registry.h"
#include "Poco/Crypto/Cipher.h"

class ModuleController
{
    ModuleController();
    ~ModuleController();

public:
    ModuleController(const ModuleController&) = delete;
    const ModuleController& operator= (const ModuleController&) = delete;
    static ModuleController& getInstance();
    bool setCredentials(std::string strUsername, std::string strPassword);
    bool validateCode(std::string strCode);
    bool validateCredentials(std::string strUsername, std::string strPassword);
    void sendImageFile(std::string pathFile, std::string username, std::string filename);
    void sendTextFile(std::string pathFile, std::string username, std::string filename);
    void receiveImageFile(std::string pathFile, std::string username, std::string fileToReceive, std::string filename);
    void receiveTextFile(std::string pathFile, std::string username, std::string fileToReceive, std::string filename);   
    void startRegister();
    
private:
    AuthorizationHandler authHandler;
    Poco::Thread threadRegister;
    Registry registryObj;
    Poco::Crypto::Cipher::Ptr pCipher;
};


#endif // MODULE_CONTROLLER_H
