#ifndef SENDER_H
#define SENDER_H

#include "Poco/Thread.h"
#include "Poco/Runnable.h"
#include "Poco/Crypto/Cipher.h"

class Sender : public Poco::Runnable
{
	
public:
	Sender();
	void setPathFile(std::string file);
	void setPortService(std::string port);
	void setIPService(std::string ip);
	virtual void run();
	void setUsername(std::string username);
	void setFilename(std::string filename);
	void setCrypto(Poco::Crypto::Cipher::Ptr &pCipher);
	
private:
	std::string encrypt(std::string &file);
	
	std::string pathFile;
	std::string ipService;
	std::string username;
	std::string filename;
	Poco::UInt16 portService;
	Poco::Crypto::Cipher::Ptr pCipher;
};
	

#endif //SENDER_H
