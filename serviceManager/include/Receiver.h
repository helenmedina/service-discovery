#ifndef RECEIVER_H
#define RECEIVER_H

#include "Poco/Thread.h"
#include "Poco/Runnable.h"
#include "Poco/Crypto/Cipher.h"


class Receiver : public Poco::Runnable
{
	
public:
	Receiver();
	void setPathFile(std::string file);
	void setPortService(std::string port);
	void setIPService(std::string ip);
	virtual void run();
	void setUsername(std::string username);
	void setFilename(std::string name);
	void setFileToReceive(std::string name);
	void setCrypto(Poco::Crypto::Cipher::Ptr &pCipher);
	void decrypt( std::string file);
	
private:
	std::string pathFile;
	std::string ipService;
	std::string username;
	std::string filename;
	std::string filenameDecoded;
	Poco::UInt16 portService;
	Poco::Crypto::Cipher::Ptr pCipher;
};
	

#endif //RECEIVER_H
