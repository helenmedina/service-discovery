#ifndef AUTHORIZATION_HANDLER_H
#define AUTHORIZATION_HANDLER_H
#include <vector>
#include <string>
#include <map>

class AuthorizationHandler
{
public:
    AuthorizationHandler();
    ~AuthorizationHandler();
    bool createUser(std::string username, std::string password);
    void removeUser(std::string username);
    std::map<std::string,std::string>::iterator findUser(std::string username) const;
    bool isCodeValid(std::string code) const;
    bool isUserValid(std::string username, std::string password) const;

private:
	const std::string random_string(std::size_t length ) const;
	const std::string encrypt(std::string &password) const;
    
    //members
    std::vector<std::string> vCodes;
    mutable std::map<std::string, std::string> mUsers;

};



#endif // AUTHORIZATION_HANDLER_H
