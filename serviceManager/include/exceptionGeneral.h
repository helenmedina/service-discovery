#ifndef EXCEPTIONS_H
#define EXCEPTIONS_H

#include <iostream>
#include <exception>
using namespace std;

class ExceptionGeneral : public std::exception
{
private:
    std::string err_msg;

public:
    ExceptionGeneral(const char *msg) : err_msg(msg)
    {}
    ~ExceptionGeneral() throw() {}
    inline const char *what () const throw () {return this->err_msg.c_str();}

};

#endif // EXCEPTIONS_H
