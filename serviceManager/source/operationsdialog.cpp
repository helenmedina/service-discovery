#include "operationsdialog.h"
#include "creationdialog.h"
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QPushButton>
#include <QLabel>
#include <QLineEdit>
#include <QGroupBox>
#include <QCheckBox>
#include <QFileDialog>
#include <QMessageBox>
#include "servicemanager.h"


OperationsDialog::OperationsDialog(ServiceManager* service)
{
    serviceManager = service;
    
	//box send file
    QGroupBox *groupSendBox = new QGroupBox(tr("Send File"));
    QVBoxLayout *vSendBox = new QVBoxLayout;
    checkBoxText = new QCheckBox(tr("Text File"));
    checkBoxImg = new QCheckBox(tr("Img File"));
    browseFile = new QPushButton("Select file");
    browseFile->setEnabled(false);
    sendTextFileButton = new QPushButton("Send");
    sendTextFileButton->setEnabled(false);

	//organize widgets in sending box
    vSendBox->addWidget(checkBoxText);
    vSendBox->addWidget(checkBoxImg);
    vSendBox->addWidget(browseFile);
    vSendBox->addWidget(sendTextFileButton);
    vSendBox->addStretch();
    groupSendBox->setLayout(vSendBox);

	//box receive file
    QGroupBox *groupReceiveBox = new QGroupBox(tr("Receive File"));
    QVBoxLayout *vReceiveBox = new QVBoxLayout;
    checkBoxTextReceive = new QCheckBox(tr("Text File"));
    checkBoxImgReceive = new QCheckBox(tr("Img File"));
    receiveFileButton = new QPushButton("Receive");
    browseDirectory = new QPushButton("Select directory");   
    
    QHBoxLayout *fileLayout = new QHBoxLayout; 
    QLabel *lbFile = new QLabel("File: ");  
    edFileName = new QLineEdit();
    fileLayout->addWidget(lbFile);
    fileLayout->addWidget(edFileName);
    
    receiveFileButton->setEnabled(false);
    browseDirectory->setEnabled(false);

	//organize widgets in receiving box
    vReceiveBox->addWidget(checkBoxTextReceive);
    vReceiveBox->addWidget(checkBoxImgReceive);
    vReceiveBox->addLayout(fileLayout);
    vReceiveBox->addWidget(browseDirectory);    
    vReceiveBox->addWidget(receiveFileButton);

    groupReceiveBox->setLayout(vReceiveBox);


    QHBoxLayout *mainLayoutH = new QHBoxLayout();
    QVBoxLayout *mainLayoutV = new QVBoxLayout();
    QPushButton *OkButton = new QPushButton("OK");

    mainLayoutH->addWidget(groupSendBox);
    mainLayoutH->addWidget(groupReceiveBox);
    
    QHBoxLayout *layoutButtonOK = new QHBoxLayout();  
    layoutButtonOK->addWidget(OkButton);
    layoutButtonOK->addStretch();
    
    mainLayoutV->addLayout(mainLayoutH);
	mainLayoutV->addLayout(layoutButtonOK);

    this->setLayout(mainLayoutV);

	//setup connections with widgets in sending and receiving boxes
    connect(checkBoxText, SIGNAL (clicked()), this, SLOT (enableBrowseButton()));
    connect(checkBoxImg, SIGNAL (clicked()), this, SLOT (enableBrowseButton()));
    connect(browseFile, SIGNAL (clicked()), this, SLOT (handleOperationBrowse()));
    connect(sendTextFileButton, SIGNAL (clicked()), this, SLOT (handleSendButton()));
    connect(sendTextFileButton, SIGNAL(clicked()), serviceManager,SLOT (SignInService()));

    connect(checkBoxTextReceive, SIGNAL (clicked()), this, SLOT (enableReceiveButton()));
    connect(checkBoxImgReceive, SIGNAL (clicked()), this, SLOT (enableReceiveButton()));
    connect(browseDirectory, SIGNAL (clicked()), this, SLOT (handleOperationDirBrowse()));
    connect(receiveFileButton, SIGNAL (clicked()), this, SLOT (handleOperationReceive()));
    connect(OkButton, SIGNAL (clicked()), this, SLOT (showMainWindow()));
}

void OperationsDialog::showMainWindow()
{
    emit showServiceManager();
    this->close();
}

void OperationsDialog::enableReceiveButton()
{
    browseDirectory->setEnabled(true);
}

void OperationsDialog::enableBrowseButton()
{
    browseFile->setEnabled(true);
}

//select the directory where the file received will be stored
void OperationsDialog::handleOperationDirBrowse()
{

    pathFile = QFileDialog::getExistingDirectory(this, tr("Open Directory"), "/home",
                                                        QFileDialog::ShowDirsOnly
                                                        | QFileDialog::DontResolveSymlinks);
    receiveFileButton->setEnabled(true);
}

QString OperationsDialog::getPathFile() const
{
	return pathFile;
}

//browse in order to select a image or text file
void OperationsDialog::handleOperationBrowse()
{
    if (checkBoxText->isChecked())
    {
        pathFile = QFileDialog::getOpenFileName(this,
            tr("Open Image"), "/home/", tr("Image Files (*.doc *.txt *.odt)"));
    }
    else if (checkBoxImg->isChecked())
    {
        pathFile = QFileDialog::getOpenFileName(this,
            tr("Open Image"), "/home/", tr("Image Files (*.jpg *.png *.jpeg)"));
    }

    sendTextFileButton->setEnabled(true);
}

//send the file selected
void OperationsDialog::handleSendButton()
{
    if (checkBoxText->isChecked())
    {
		emit sendTextFile();
    }
    else if (checkBoxImg->isChecked())
    {
		emit sendImageFile();
    }
    
}
//receive the file specified
void OperationsDialog::handleOperationReceive()
{
	fileNameToReceive = edFileName->text();
	if(fileNameToReceive.toStdString().empty())
	{
		QMessageBox msgBox;
        msgBox.setText("File cannot be empty.");
        msgBox.exec();
	}
	else
	{
		if (checkBoxTextReceive->isChecked())
		{
			emit receiveTextFile();
		}
		else if (checkBoxImgReceive->isChecked())
		{
			emit receiveImageFile();
		}
	}
}

QString OperationsDialog::getFileNameToReceive() const
{
	return fileNameToReceive;
}
