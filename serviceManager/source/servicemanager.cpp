#include "servicemanager.h"
#include "ui_mainwindow.h"
#include <QDialog>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QPushButton>
#include <QMessageBox>
#include <QLabel>
#include <QLineEdit>
#include <QFile>
#include "creationdialog.h"
#include "signindialog.h"
#include "operationsdialog.h"
#include "modulecontroller.h"
#include "exceptionGeneral.h"
#include <iostream>

ServiceManager::ServiceManager(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    creationDialog = new CreationDialog();
    signDialog = new SignInDialog();
    operations = new OperationsDialog(this);
    
    //reset values
    userIdentifier = 0;
    fileIdentifier = 0;
    userList.clear();

	//start registry service to listen for incoming requests
    initRegister();

	//setup connections
    connect(creationDialog, SIGNAL (sendDataToController()),this, SLOT (sendDataToController()));
    connect(signDialog, SIGNAL (checkCredentialsInController()),this, SLOT (SignInService()));
    connect(this, SIGNAL (closeSignInDialog()),signDialog, SLOT (close()));
    connect(this, SIGNAL (closeCreationDialog()),creationDialog, SLOT (close()));
    connect(operations, SIGNAL (sendImageFile()),this, SLOT (sendImageFile()));
    connect(operations, SIGNAL (sendTextFile()),this, SLOT (sendTextFile()));
    connect(operations, SIGNAL (receiveImageFile()),this, SLOT (receiveImageFile()));
    connect(operations, SIGNAL (receiveTextFile()),this, SLOT (receiveTextFile()));
    connect(operations, SIGNAL (showServiceManager()),this, SLOT (showServiceManager()));
        
}

/*****************************************
 * 
 * Make the service manager visible
 * 
 * ***************************************/
void ServiceManager::showServiceManager()
{
	this->show();
}

/*****************************************
 * 
 * Starts the registry to listen for incoming
 * request of other services
 * 
 * ***************************************/
void ServiceManager::initRegister()
{
	ModuleController::getInstance().startRegister();
}

ServiceManager::~ServiceManager()
{

    delete ui;
}

/*****************************************
 * 
 * slot SignInService that checks if user
 * and password are valid
 * 
 * ***************************************/
void ServiceManager::SignInService()
{
    if (!ModuleController::getInstance().validateCredentials(signDialog->getUsername().toStdString(), signDialog->getPassword().toStdString()))
    {
        QMessageBox msgBox;
        msgBox.setText("The user or the password is not valid.");
        msgBox.exec();
    }
    else
    {
        emit closeSignInDialog();
        this->hide();
        operations->show();
    }
}

/*****************************************
 * 
 * Extracts the file name from path file
 * 
 * ***************************************/

QString ServiceManager::getFileName(QString &pathFile) const
{
	
	QStringList parts = pathFile.split("/");
	QString filename = parts.at(parts.size()-1);

	return filename;
}

/*******************************************************
 * 
 * Look for user in the userList. If user is not found it
 * throws an exception
 * 
 * ****************************************************/
std::string ServiceManager::getUserFromList(std::string user)
{
	std::map<std::string, std::string>::iterator it;
	it = userList.find(user);
	if (it != userList.end())
	{
		return it->second;
	}
	else
	{
		throw ExceptionGeneral("Error finding user in list.");
	}
}

/***********************************************************
 * 
 * Look for file in the fileList. If user is not found it
 * throws an exception
 * 
 * ********************************************************/
std::string ServiceManager::getFileFromList(std::string file)
{
	std::map<std::string, std::string>::iterator it;
	it = fileList.find(file);
	if (it != fileList.end())
	{
		return it->second;
	}
	else
	{
		throw ExceptionGeneral("Error finding file in list.");
	}
}

/***********************************************************
 * 
 * Build the file identifier and send the image file to the 
 * image store service. 
 * throws an exception if file or user identifier is not found
 * 
 * ********************************************************/
void ServiceManager::sendImageFile()
{
	QString pathFile = operations->getPathFile();
	QString filename = getFileName(pathFile);

	try
	{
		std::string strUser = getUserFromList(signDialog->getUsername().toStdString());
		//build a temporal id (user id + _ + file name) to save it in fileList where user id is unique
		std::string tmp = strUser + "_" + filename.toStdString();
		//save id in fileList for the image file
		setFileIdentifier(tmp);
		//gets file id 
		std::string fileToSend = getFileFromList(tmp);
		

		ModuleController::getInstance().sendImageFile(pathFile.toStdString(), strUser, fileToSend);
	}
	catch(ExceptionGeneral& e)
	{
		std::cout << e.what() << std::endl;
	}

}

/***********************************************************
 * 
 * Build the file identifier and send the text file to the 
 * text store service. 
 * throws an exception if file or user identifier is not found
 * 
 * ********************************************************/
void ServiceManager::sendTextFile()
{
	QString pathFile = operations->getPathFile();
	QString filename = getFileName(pathFile);

	try
	{
		std::string strUser = getUserFromList(signDialog->getUsername().toStdString());
		
		std::string tmp = strUser + "_" + filename.toStdString();
		//save id in fileList for the text file
		setFileIdentifier(tmp);
		//gets file id 
		std::string fileToSend = getFileFromList(tmp);

		ModuleController::getInstance().sendTextFile(pathFile.toStdString(), strUser, fileToSend);
	}
	catch(ExceptionGeneral& e)
	{
		std::cout << e.what() << std::endl;
	}	
		
}

/***********************************************************
 * 
 * Get file identifier and connect with image store service 
 * to receive file
 * throws an exception if file or user identifier is not found
 * 
 * ********************************************************/
void ServiceManager::receiveImageFile()
{
	
	QString pathFile = operations->getPathFile();
	QString filename = operations->getFileNameToReceive();

	try
	{
		//get user name from userList
		std::string strUser = getUserFromList(signDialog->getUsername().toStdString());		
		std::string tmp = strUser + "_" + filename.toStdString();
		//gets file id 
		std::string fileToSend = getFileFromList(tmp);

		ModuleController::getInstance().receiveImageFile(pathFile.toStdString(), strUser, fileToSend, filename.toStdString());

	}
	catch(ExceptionGeneral& e)
	{
		std::cout << e.what() << std::endl;
	}	

}

/***********************************************************
 * 
 * Build the file identifier and send the text file to the 
 * text store service. 
 * throws an exception if file or user identifier is not found
 * 
 * ********************************************************/
void ServiceManager::receiveTextFile()
{
	QString pathFile = operations->getPathFile();	
	QString filename = operations->getFileNameToReceive();

	try
	{
		//get user name from userList
		std::string strUser = getUserFromList(signDialog->getUsername().toStdString());		
		std::string tmp = strUser + "_" + filename.toStdString();
		//gets file id 
		std::string fileToSend = getFileFromList(tmp);

		ModuleController::getInstance().receiveTextFile(pathFile.toStdString(), strUser, fileToSend, filename.toStdString());

	}
	catch(ExceptionGeneral& e)
	{
		std::cout << e.what() << std::endl;
	}
	
}

/***********************************************************
 * 
 * Add a new user in the userList if number of users is lower 
 * or equal to USER_IDENTIFIER_MAX
 * 
 * ********************************************************/
bool ServiceManager::setUserIdentifierValid(QString user)
{
	long int userId = ++userIdentifier;
	if (userId > USER_IDENTIFIER_MAX)
	{
		QMessageBox msgBox;
        msgBox.setText("No more users allowed");
        msgBox.exec();
        
        return false;
	}
	else
	{		
		QString strUser = QString::number(userId).rightJustified(USER_MAX_SIZE, '0');
		userList.insert ( std::pair<std::string, std::string>(user.toStdString(), strUser.toStdString()) );
		
		return true;
	}
}

/***********************************************************
 * 
 * Add a new file in the fileList if number of files is lower 
 * or equal to FILE_IDENTIFIER_MAX
 * 
 * ********************************************************/
void ServiceManager::setFileIdentifier(std::string filename)
{
	long int fileId = ++fileIdentifier;
	
	if (fileId > FILE_IDENTIFIER_MAX)
	{
		--fileIdentifier;
		QMessageBox msgBox;
        msgBox.setText("No more files to save allowed");
        msgBox.exec();
	}
	else
	{		
		QString strFile = QString::number(fileId).rightJustified(FILE_MAX_SIZE, '0');
		
		std::map<std::string, std::string>::iterator it;
		it = fileList.find(filename);
		if (it == fileList.end())
		{
			fileList.insert ( std::pair<std::string, std::string>(filename, strFile.toStdString()) );
		}

	}
}

/***********************************************************
 * 
 * Creates a new user if credentials are valid
 * 
 * ********************************************************/
void ServiceManager::sendDataToController()
{
   
    //close and open dialog
	QString user = creationDialog->getUserName();
	if(setUserIdentifierValid(user))
	{
		QString name = creationDialog->getName();   
		QString password = creationDialog->getPassword();
		QString code = creationDialog->getCode();
		
		//send user, password and code to register user in the system
		if (!(ModuleController::getInstance().validateCode(code.toStdString())))
		{
			QMessageBox msgBox;
			msgBox.setText("The code is not valid. Please try again.");
			msgBox.exec();
		}
		else
		{
			//create user if code is valid
			if (!ModuleController::getInstance().setCredentials(user.toStdString(), password.toStdString()))
			{
				QMessageBox msgBox;
				msgBox.setText("User already exist.");
				msgBox.exec();
			}
			else
			{
				emit closeCreationDialog();
				QMessageBox msgBox;
				msgBox.setText("User created.");
				msgBox.exec();
			}

		}
	}

}

void ServiceManager::on_createAccountButton_clicked()
{
    creationDialog->setWindowTitle("Creation of an user account");
    
    creationDialog->show();
}

void ServiceManager::on_signButton_clicked()
{
    signDialog->setWindowTitle("Sign in");

    signDialog->show();

}
