
#include "Poco/Net/StreamSocket.h"
#include "Poco/Net/ServerSocket.h"
#include "Poco/Net/SocketStream.h"
#include "Poco/Net/SocketAddress.h"
#include "Sender.h"
#include "Poco/StreamCopier.h"
#include "Poco/Timespan.h"
#include "Poco/FileStream.h"
#include "Poco/Crypto/Cipher.h"
#include "Poco/Crypto/CipherFactory.h"
#include "Poco/Crypto/CipherKey.h"
#include "Poco/Crypto/CryptoStream.h"
#include "Poco/Exception.h"
#include "Poco/Crypto/CryptoException.h"
#include "Poco/Crypto/CryptoTransform.h"
#include <sstream>
#include <iostream>

Sender::Sender()
{}


/***********************************************************
 * 
 * Opens a TCP stream socket to send a file to imageStore
 * or textStore services
 * 
 * *********************************************************/
void Sender::run()
{
	std::cout << "******Starting sender process******" << std::endl;
	
	std::cout << "ip: " << ipService << " port: "  <<  portService << std::endl;
	//Creates a socket for an ip and port
	Poco::Net::SocketAddress sa(ipService, portService);
	//tcp stream socket
	Poco::Net::StreamSocket ss1(sa);
	//An bidirectional stream for reading from and writing to a socket
	Poco::Net::SocketStream ostr(ss1);
	
	//Define type operation, user identifier and file identifier
	ostr << "send";
	ostr << "id" << username;
	ostr << "file" << filename;
	
	//Encrypt file and sen it to a specific service 
	Poco::FileInputStream istr(encrypt(pathFile), std::ios::in);
	//copy datastream to the socket
	Poco::StreamCopier::copyStream(istr, ostr);
	std::cout << "The file has been sent" << std::endl;
	ostr.flush();
	
}

/***********************************************************
 * 
 * Encrypt a file using the cipher aes256, based on crypto
 * library
 * Throws CryptoException and Excetion if error is produced
 * 
 * *********************************************************/
std::string Sender::encrypt(std::string &file)
{
	//path file where the file encrypted will be saved
	std::string fileEncrypted = "./encrypted.txt";
	try
	{
		//An output stream for writing to a file
		Poco::FileOutputStream sink(fileEncrypted);
		//creates an encryptor
		Poco::Crypto::CryptoTransform* pEncryptor = pCipher->createEncryptor();		
		Poco::Crypto::CryptoOutputStream encryptorStream(sink, pEncryptor);
		//input file
		Poco::FileInputStream source(file);
		//encrypt the input file
		Poco::StreamCopier::copyStream(source, encryptorStream);
		
		encryptorStream.close();
		sink.close();
		
		std::cout << "file encrypted"  <<std::endl;
		
	
	}
	catch(Poco::Crypto::CryptoException &e)
	{
		std::cout << "Error: " <<  e.what() <<std::endl;
	}
	catch(Poco::Exception &e)
	{
		std::cout << "Error: " <<  e.what() <<std::endl;
	}

	return fileEncrypted;

}

void Sender::setPathFile(std::string file)
{
	pathFile = file;
}

void Sender::setPortService(std::string port)
{
	unsigned short portShort = stoi(port);
	portService = portShort;
}

void Sender::setIPService(std::string ip)
{
	ipService = ip;
}

void Sender::setUsername(std::string name)
{
	username = name;
}

void Sender::setFilename(std::string name)
{
	filename = name;
}
void Sender::setCrypto(Poco::Crypto::Cipher::Ptr &pCipher)
{
	this->pCipher = pCipher;
}
