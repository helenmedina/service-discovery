
#include "Poco/Net/StreamSocket.h"
#include "Poco/Net/ServerSocket.h"
#include "Poco/Net/SocketStream.h"
#include "Poco/Net/DialogSocket.h"
#include <iostream>
#include "Poco/Net/SocketAddress.h"
#include "Receiver.h"
#include "Poco/StreamCopier.h"
#include "Poco/Timespan.h"
#include "Poco/FileStream.h"
#include "Poco/Crypto/CryptoStream.h"
#include "Poco/Exception.h"
#include "Poco/Crypto/CryptoException.h"
#include "Poco/Crypto/CryptoTransform.h"

Receiver::Receiver()
{}

void Receiver::run()
{
	/*std::cout << "****** Starting receiver process ******" << std::endl;
	//Creates a socket for an ip and port
	Poco::Net::SocketAddress sa(ipService, portService);
	std::cout << "ip: " << ipService << " port: "  <<  portService << std::endl;
	//tcp stream socket
	Poco::Net::StreamSocket ss1(sa);	
	//An bidirectional stream for reading from and writing to a socket
	Poco::Net::SocketStream ostr(ss1);
	
	//Define type operation, user identifier and file identifier
	ostr << "receiver";
	ostr << "id" << username;
	ostr << "file" << filename;

	ostr.flush();
	
	std::cout << "****** Waiting to receive data ******" << std::endl;*/
	
	std::string fileToReceive = pathFile + "/" + username + "_" + filename ;

	//Copy datastream to file 
	/*Poco::FileOutputStream streamToReceive(fileToReceive, std::ios::binary);
	Poco::StreamCopier::copyStream(ostr, streamToReceive);
	ostr.flush();*/
	
	//decrypt file
	decrypt(fileToReceive);	

}

void Receiver::decrypt( std::string file)
{
	try
	{
		Poco::Crypto::CryptoTransform* pDecryptor = pCipher->createDecryptor();
		//Poco::FileInputStream EncryptedSource(fileToReceive);
		Poco::FileInputStream EncryptedSource("/media/helen/Data/Applications/Bitbucket/ServiceDiscovery/service-discovery/build/app_store_img/000001_0000000001");
		Poco::Crypto::CryptoInputStream decryptorStream(EncryptedSource, pDecryptor);
		//save decrypted file in path specified for user
		std::string fileDecoded = pathFile + "/" + filenameDecoded;
		Poco::FileOutputStream result(fileDecoded);
		//copy decrypted data to file
		Poco::StreamCopier::copyStream(decryptorStream, result);
		
		std::cout << "file decrypted successfully: "  << fileDecoded <<std::endl;
	}
	catch(Poco::Crypto::CryptoException &e)
	{
		std::cout << "Error: " <<  e.what() <<std::endl;
	}
	catch(Poco::Exception &e)
	{
		std::cout << "Error: " <<  e.what() <<std::endl;
	}

}

void Receiver::setPathFile(std::string file)
{
	pathFile = file;
}

void Receiver::setPortService(std::string port)
{
	unsigned short portShort = stoi(port);
	portService = portShort;
}

void Receiver::setIPService(std::string ip)
{
	ipService = ip;
}

void Receiver::setUsername(std::string name)
{
	username = name;
}

void Receiver::setFileToReceive(std::string name)
{
	filename = name;
}
void Receiver::setFilename(std::string name)
{
	filenameDecoded = name;
}

void Receiver::setCrypto(Poco::Crypto::Cipher::Ptr &pCipher)
{
	this->pCipher = pCipher;
}
