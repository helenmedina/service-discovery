#include "creationdialog.h"
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QPushButton>
#include <QLabel>
#include <QLineEdit>
#include <QMessageBox>
#include <QRegularExpression>

CreationDialog::CreationDialog()
{
    QHBoxLayout *layoutName = new QHBoxLayout();
    QHBoxLayout *layoutUserName = new QHBoxLayout();
    QHBoxLayout *layoutPassword = new QHBoxLayout();
    QHBoxLayout *layoutConfirmationPassword = new QHBoxLayout();
    QHBoxLayout *layoutCode = new QHBoxLayout();
    QHBoxLayout *layoutButton = new QHBoxLayout();

    initElements();

	//organize widgest in layouts for better visibility
	//name field
    layoutName->addWidget(lbName);
    layoutName->addWidget(edName);
    //username field
    layoutUserName->addWidget(lbUserName);
    layoutUserName->addWidget(edUserName);
    //password field
    layoutPassword->addWidget(lbPassword);
    layoutPassword->addWidget(edPassword); 
    //Confirmation pasword field 
    layoutConfirmationPassword->addWidget(lbConfirmationPassword);
    layoutConfirmationPassword->addWidget(edConfirmationPassword); 
    //code field
    layoutCode->addWidget(lbCode);
    layoutCode->addWidget(edCode);
    layoutButton->addStretch(5);
    
    //button field
    layoutButton->addWidget(btnOk);
    layoutButton->addWidget(btnCancel);

    QVBoxLayout *mainLayout = new QVBoxLayout();
    mainLayout->addLayout(layoutName);
    mainLayout->addLayout(layoutUserName);
    mainLayout->addLayout(layoutPassword);
    mainLayout->addLayout(layoutConfirmationPassword);
    mainLayout->addLayout(layoutCode);
    mainLayout->addLayout(layoutButton);

    this->setLayout(mainLayout);
}

void CreationDialog::initElements()
{
    lbName = new QLabel("Name: ");
    lbName->setFixedWidth(130);
    edName = new QLineEdit("");
    lbUserName = new QLabel("User: ");
    lbUserName->setFixedWidth(130);
    edUserName = new QLineEdit();
    lbPassword = new QLabel("Password: ");
    lbPassword->setFixedWidth(130);
    edPassword = new QLineEdit();
    edPassword->setEchoMode(QLineEdit::Password);
    lbConfirmationPassword = new QLabel("Confirm password: ");
    lbConfirmationPassword->setFixedWidth(130);
    edConfirmationPassword = new QLineEdit();
    edConfirmationPassword->setEchoMode(QLineEdit::Password);
    lbCode = new QLabel("Verification code: ");
    lbCode->setFixedWidth(130);
    edCode = new QLineEdit();
    btnOk = new QPushButton("OK");
    btnCancel = new QPushButton("Cancel");
    
    //setup connections
    connect(btnOk, SIGNAL (clicked()),this, SLOT (saveData()));
    connect(btnCancel, SIGNAL (clicked()),this, SLOT (close()));
}

void CreationDialog::saveData()
{
    name = edName->text();
    userName = edUserName->text();
    password = edPassword->text();
    
    if (validatePassword(password))
    {
		confirmationPassword = edConfirmationPassword->text();
		code = edCode->text();

		if (password.isEmpty() || userName.isEmpty() || code.isEmpty() )
		{
			QMessageBox msgBox;
			msgBox.setText("The fields cannot be empty");
			msgBox.exec();
		}
		if (confirmationPassword != password )
		{
			QMessageBox msgBox;
			msgBox.setText("The pasword does not match");
			msgBox.exec();
		}
		emit sendDataToController();
	}
	else
	{
			QMessageBox msgBox;
			msgBox.setText("Password must verify following requirements: \n"
			"Passwords will contain at least 1 upper case letter \n"
			"Passwords will contain at least 1 lower case letter \n"
			"Passwords will contain at least 1 number or special character \n"
			"Passwords will contain at least 8 characters in length");
			msgBox.exec();
	}

}

bool CreationDialog::validatePassword(QString password)
{
	QRegularExpression rx("(?=^.{8,}$)((?=.*\\d)|(?=.*\\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$");
	bool bValid = rx.match(password).hasMatch();
	
	return bValid;
}

QString CreationDialog::getName() const
{
    return name;
}
QString CreationDialog::getUserName() const
{
    return userName;
}
QString CreationDialog::getPassword() const
{
    return password;
}
QString CreationDialog::getCode() const
{
    return code;
}
