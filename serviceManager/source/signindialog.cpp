#include "signindialog.h"
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QPushButton>
#include <QLabel>
#include <QLineEdit>

SignInDialog::SignInDialog()
{
	//organize widgets for better visibility
    QHBoxLayout *layoutUserName = new QHBoxLayout();
    QHBoxLayout *layoutPassword = new QHBoxLayout();
    QHBoxLayout *layoutButton = new QHBoxLayout();

	//username field
    QLabel *lbUserName = new QLabel("User: ");
    lbUserName->setFixedWidth(130);
    edUserName = new QLineEdit();
    layoutUserName->addWidget(lbUserName);
    layoutUserName->addWidget(edUserName);
    
    //password field
    QLabel *lbPassword = new QLabel("Password: ");
    lbPassword->setFixedWidth(130);
    edPassword = new QLineEdit();
    layoutPassword->addWidget(lbPassword);
    layoutPassword->addWidget(edPassword);
    
    //button field
    edPassword->setEchoMode(QLineEdit::Password);
    QPushButton *btnOk = new QPushButton("OK");
    QPushButton *btnCancel = new QPushButton("Cancel");
    layoutButton->addStretch(5);
    layoutButton->addWidget(btnOk);
    layoutButton->addWidget(btnCancel);

    QVBoxLayout *mainLayout = new QVBoxLayout();

    mainLayout->addLayout(layoutUserName);
    mainLayout->addLayout(layoutPassword);
    mainLayout->addLayout(layoutButton);

    this->setLayout(mainLayout);
    connect(btnOk, SIGNAL (clicked()),this, SLOT (checkCredentials()));
}

void SignInDialog::checkCredentials()
{
    emit checkCredentialsInController();
}

QString SignInDialog::getUsername() const
{
    return edUserName->text();
}

QString SignInDialog::getPassword() const
{
    return edPassword->text();
}
