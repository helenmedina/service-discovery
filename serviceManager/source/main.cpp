#include "servicemanager.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    ServiceManager w;
    w.show();

    return a.exec();
}
