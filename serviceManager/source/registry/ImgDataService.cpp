#include "ImgDataService.h"
#include <iostream>

ImgDataService::ImgDataService(const char* port, const char* ip) : IService(port, ip)
{
}

ImgDataService::ImgDataService()
{
}

ImgDataService::~ImgDataService()
{
	std::cout << "imageService deleted" << std::endl;
}
