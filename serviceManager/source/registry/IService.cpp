#include "IService.h"
#include <cstdlib>
#include <cstring>

/*********************************************************
 * 
 * Defines the interface of a service to start listening
 * for incoming requests and sending information to service
 * manager
 *  
 * *******************************************************/
IService::IService( const char* port, const char* ip) 
{
	setPort(port);
	setIP(ip);
}

IService::~IService(){}

const char* IService::getPort() const
{
	return m_port;
}

const char* IService::getIP() const
{
	return m_ip;
}

void IService::setPort(const char* port)
{
	m_port = (char*)malloc(sizeof(port));
	std::strcpy (m_port, port);
}

void IService::setIP(const char* ip)
{
	m_ip = (char*)malloc(sizeof(ip));
	std::strcpy (m_ip, ip);
}
