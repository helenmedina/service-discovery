#include "ServiceFactory.h"
#include "TextDataService.h"
#include "ImgDataService.h"
#include "IService.h"
#include <memory>
#include <cstring>

/**********************************************************************
 * 
 * Class factory that creates a specific service depending on service 
 * type.
 * @return service, it type is unkown return null
 * 
 * ********************************************************************/
std::unique_ptr<IService> ServiceFactory::createService(const char *type)
{
	if (std::strcmp(type, "TextStore") == 0)
	{
		std::unique_ptr<IService> textService( new TextDataService() );
		return textService;
	}
	else if (std::strcmp(type, "ImgStore") == 0)
	{
		std::unique_ptr<IService> imgService( new ImgDataService() );
		return imgService;
	}
	else
		return nullptr;
}
