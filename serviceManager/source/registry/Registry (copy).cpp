#include "Registry.h"
#include "TextDataService.h"
#include "Poco/Net/StreamSocket.h"
#include "Poco/Net/ServerSocket.h"
#include <iostream>
#include <memory>
#include <map>
#include "Poco/Net/KeyFileHandler.h"
#include "Poco/Net/ConsoleCertificateHandler.h"
#include "Poco/Net/Context.h"
#include "Poco/Net/SocketAddress.h"

Registry::Registry()
{
	servicesRegistered.clear();
	stop  = false;
}
void Registry::run()
{
	
	Poco::Net::ServerSocket svr(9912); //bind + listen	

	while (!stop)
	{
			
		std::cout << "Conectado" << std::endl;
		Poco::Net::StreamSocket ss = svr.acceptConnection(); //accept connection
		
		size_t buf_sz = 1000;
		std::unique_ptr<char[]> buffer(new char[buf_sz]);
		int num = ss.receiveBytes(buffer.get(), buf_sz);
		std:: string msg(buffer.get());

		std::cout << "msg "<< msg << std::endl;
		
		HandleRequest(msg);		
		
	}
	
	
	//get pointer to derived class

	/*std::unique_ptr<TextDataService> derivedPointer;
	
	
	TextDataService *tmp = dynamic_cast<TextDataService*>(textService.get());
	if(tmp != nullptr)
	{
		textService.release();
		derivedPointer.reset(tmp);
		registerService(std::make_shared<IService>(std::move(derivedPointer)), eServicesType::TextService);
		//registerService(derivedPointer, eServicesType::TextService);
		//getService (eServicesType::TextService);
		
		//derivedPointer->testPrint();
	}*/
	

	
/*	std::shared_ptr<IService> shared = std::move(textService);
	std::cout << shared->getPort() << std::endl;
	registerService(shared, eServicesType::TextService);
	
	std::shared_ptr<IService> shared2 = getService (eServicesType::TextService);
	
	std::cout << "get service" << shared2->getPort() << std::endl;
	
	
	
	/std::cout << "dymanic" << std::endl;
	//if(shared2)
	//{
	//	IService *testp = shared2.get();
	//	TextDataService * dest = dynamic_cast<TextDataService*> (testp);
	//	std::cout << "not null" << std::endl;
	//}/
	std::shared_ptr<TextDataService> tmp = std::dynamic_pointer_cast<TextDataService>(shared2);
	std::cout << "dimanic" << std::endl;
	if(tmp != nullptr)
	{
		 tmp->testPrint();
	}
	
	
	unregisterService(eServicesType::TextService);
	std::shared_ptr<IService> shared3 = getService (eServicesType::TextService);
	if(shared3)
	{
		std::cout << "SHARED NOT NULL" << std::endl;
	}
	else
		std::cout << "SHARED is NULL" << std::endl;
	*/
	
}

void Registry::registerService (std::shared_ptr<IService> service, eServicesType serviceID)
{
	if (serviceID != eServicesType::None)
	{
		servicesRegistered.insert(std::pair<eServicesType,std::shared_ptr<IService>>(serviceID, service));
		std::cout << "Service registered in port: " << service->getPort() << std::endl;
	}
	else
		std::cout << "Service unknow not registered" << std::endl;

}


void Registry::unregisterService(eServicesType serviceID)
{
	auto it = findService(serviceID);
	if (it != servicesRegistered.end())
		servicesRegistered.erase(it);
}

	
std::map<eServicesType, std::shared_ptr<IService>>::iterator Registry::findService(eServicesType serviceID)
{
	std::map<eServicesType, std::shared_ptr<IService>>::iterator it;
	it = servicesRegistered.find(serviceID);
	
	return it;
}

std::shared_ptr<IService> Registry::getService (eServicesType serviceID)
{
	std::map<eServicesType, std::shared_ptr<IService>>::iterator it;
	bool bFound(false);

	for (it = servicesRegistered.begin(); it!=servicesRegistered.end() && !bFound; )
	{
		if (it->first == serviceID)
		{
			bFound = true;	
		}
		else
			it++;
	}
	if (bFound)
	{
		
		std::cout << "service found" << std::endl;

		return it->second;

	}
	else
	{
		std::cout << "service not found" << std::endl;
		return std::shared_ptr<IService>();
	}
	
	
	
}


void Registry::parseMsg(std::map<std::string, std::string>& buffer, std::string delimiter, std::string msg)
{
	
	size_t pos = 0;
	std::string token;
	
	while ((pos = msg.find(delimiter)) != std::string::npos) {
		size_t pos_token = 0;
		token = msg.substr(0, pos);
		pos_token = token.find(":");
		std::string type = token.substr(0, pos_token);
		std::string command = token.substr(pos_token + 1, std::string::npos);
		std::pair<std::map<std::string, std::string>::iterator,bool> ret;
		ret = buffer.insert ( std::pair<std::string, std::string>(type, command) );
		
		//std::cout << msg << std::endl;
		//std::cout << "type " << type << " command " << command << std::endl;
		if (ret.second==false) {
			std::cout << "element already existed";
		}
				
		msg.erase(0, pos + delimiter.length());
	}
	
}

std::string Registry::findElement(const std::map<std::string, std::string> &buffer, std::string element)
{
	//std::map<std::string, std::string>::iterator it = buffer.find(element);
	auto it = buffer.find(element);
	
	if (it != buffer.end())
		return it->second;
	else
		return std::string();
}

eServicesType Registry::getServiceType(std::string strService) 
{
	if (strService == "TextStore")
		return eServicesType::TextService;
	else if (strService == "ImgStore")
		return eServicesType::ImgService;
	else
		return eServicesType::None;
}

void Registry::buildServiceAndRegister(std::map<std::string, std::string>& buffer)
{
	std::unique_ptr<ServiceFactory> p( new ServiceFactory() );
	std::string strService = findElement(buffer, "service");
	if (!strService.empty())
	{
		//raise exception
		std::unique_ptr<IService> service = p->createService(strService.c_str());
		if (service != nullptr)
		{
			std::string strPort = findElement(buffer, "port");
			std::string strIp = findElement(buffer, "ip");
			service->setPort(strPort.c_str());
			service->setIP(strIp.c_str());
			//std::cout << "port " << service->getPort() << std::endl;
			//std::cout << "ip "<< service->getIP() << std::endl;
			std::shared_ptr<IService> shared = std::move(service);
			registerService(shared, getServiceType(strService));
		}
	}
	else
		std::cout << "Service type does not exist " << std::endl;
	
}

void Registry::removeService(const std::map<std::string, std::string>& buffer)
{
	std::string strService = findElement(buffer, "service");
	unregisterService(getServiceType(strService));
}

void Registry::HandleRequest(std::string &msg)
{
	std::map<std::string, std::string> buffer;
	parseMsg(buffer, " ", msg);
	
	std::string strType = findElement(buffer, "type");
	if (!strType.empty())
	{
		
		if (strType == "register")
		{
			buildServiceAndRegister(buffer);
		}
		else if (strType == "unregister")
		{		
			removeService(buffer);
		}
		else
			std::cout << "Command not available: " <<  strType << std::endl;
		
	}
	else
		std::cout << "Service is not created" << std::endl;
	
	std::cout << "Operation: " <<  strType << std::endl;
	
}

//echo "type:register service:TextStore port:8080 ip:127.0.0.2 " | nc localhost 9911
//echo "type:unregister service:TextStore " | nc localhost 9911
//sudo netstat -plnt | grep ':9911'
//export LD_LIBRARY_PATH=/media/helen/Data/Applications/Bitbucket/ServiceDiscovery/service-discovery/dependencies/Poco/lib
