#include "Registry.h"
#include "TextDataService.h"
#include "Poco/Net/StreamSocket.h"
#include "Poco/Net/ServerSocket.h"
#include "Poco/Net/KeyFileHandler.h"
#include "Poco/Net/ConsoleCertificateHandler.h"
#include "Poco/Net/Context.h"
#include "Poco/Net/SocketAddress.h"
#include <iostream>
#include <memory>
#include <map>


Registry::Registry()
{
	servicesRegistered.clear();
	stop  = false;
}

/**************************************
 * 
 * Start registry to listen for incoming 
 * request in the port 9912
 * 
 * ***********************************/

void Registry::run()
{
	//bind + listen	
	Poco::Net::ServerSocket svr(REGISTRY_PORT); 

	while (!stop)
	{
			
		std::cout << "Registry listening..." << std::endl;
		Poco::Net::StreamSocket ss = svr.acceptConnection(); //accept connection
		
		size_t buf_sz = BUFFER_SIZE;
		std::unique_ptr<char[]> buffer(new char[buf_sz]);
		ss.receiveBytes(buffer.get(), buf_sz);
		std:: string msg(buffer.get());

		std::cout << "request: "<< msg << std::endl;
		
		HandleRequest(msg);		
		
	}
	
}

/**************************************
 * 
 * Register the service in registry
 * 
 * ***********************************/
void Registry::registerService (std::shared_ptr<IService> service, eServicesType serviceID)
{
	if (serviceID != eServicesType::None)
	{
		servicesRegistered.insert(std::pair<eServicesType,std::shared_ptr<IService>>(serviceID, service));
		std::cout << "Service registered in port: " << service->getPort() << std::endl;
	}
	else
		std::cout << "Service unknow not registered" << std::endl;

}

/**************************************
 * 
 * Unregister the service in registry
 * 
 * ***********************************/
void Registry::unregisterService(eServicesType serviceID)
{
	auto it = findService(serviceID);
	if (it != servicesRegistered.end())
		servicesRegistered.erase(it);
}

/**********************************************
 * 
 * Find the service in registry
 * @param type service
 * @returns iterator to service, if it not found 
 * it returns null
 * *********************************************/	
std::map<eServicesType, std::shared_ptr<IService>>::iterator Registry::findService(eServicesType serviceID)
{
	std::map<eServicesType, std::shared_ptr<IService>>::iterator it;
	it = servicesRegistered.find(serviceID);
	
	return it;
}

/**********************************************
 * 
 * Returns the registered service
 * @param type service
 * @returns iterator a smart pointer to service,
 * if it is not found it returns null 
 * *********************************************/	
std::shared_ptr<IService> Registry::getService (eServicesType serviceID)
{
	std::map<eServicesType, std::shared_ptr<IService>>::iterator it;
	bool bFound(false);

	for (it = servicesRegistered.begin(); it!=servicesRegistered.end() && !bFound; )
	{
		if (it->first == serviceID)
		{
			bFound = true;	
		}
		else
			it++;
	}
	if (bFound)
	{
		
		std::cout << "service found" << std::endl;

		return it->second;

	}
	else
	{
		std::cout << "service not found" << std::endl;
		return std::shared_ptr<IService>();
	}
	
}
/**********************************************************************************************************
 * 
 * Parse the request and look for the type operation: register and unregister, ip, port and service type
 * @param message received
 * @param delimiter space
 * @param buffer that stores the operation type
 * ********************************************************************************************************/

void Registry::parseMsg(std::map<std::string, std::string>& buffer, std::string delimiter, std::string msg)
{
	
	size_t pos = 0;
	std::string token;
	
	while ((pos = msg.find(delimiter)) != std::string::npos) {
		size_t pos_token = 0;
		token = msg.substr(0, pos);
		pos_token = token.find(":");
		std::string type = token.substr(0, pos_token);
		std::string command = token.substr(pos_token + 1, std::string::npos);
		std::pair<std::map<std::string, std::string>::iterator,bool> ret;
		ret = buffer.insert ( std::pair<std::string, std::string>(type, command) );
		
		if (ret.second==false) {
			std::cout << "element already existed";
		}
				
		msg.erase(0, pos + delimiter.length());
	}
	
}

std::string Registry::findElement(const std::map<std::string, std::string> &buffer, std::string element)
{
	auto it = buffer.find(element);
	
	if (it != buffer.end())
		return it->second;
	else
		return std::string();
}

eServicesType Registry::getServiceType(std::string strService) 
{
	if (strService == "TextStore")
		return eServicesType::TextService;
	else if (strService == "ImgStore")
		return eServicesType::ImgService;
	else
		return eServicesType::None;
}

/******************************************************************************
 * 
 * A service object is created and registered if the service type is 
 * TextStore or ImgStore
 * 
 * ****************************************************************************/
void Registry::buildServiceAndRegister(std::map<std::string, std::string>& buffer)
{
	std::unique_ptr<ServiceFactory> p( new ServiceFactory() );
	std::string strService = findElement(buffer, "service");
	if (!strService.empty())
	{
		std::unique_ptr<IService> service = p->createService(strService.c_str());
		if (service != nullptr)
		{
			std::string strPort = findElement(buffer, "port");
			std::string strIp = findElement(buffer, "ip");
			service->setPort(strPort.c_str());
			service->setIP(strIp.c_str());
			std::shared_ptr<IService> shared = std::move(service);
			registerService(shared, getServiceType(strService));
		}
	}
	else
		std::cout << "Service type does not exist " << std::endl;
	
}

void Registry::removeService(const std::map<std::string, std::string>& buffer)
{
	std::string strService = findElement(buffer, "service");
	unregisterService(getServiceType(strService));
}

/********************************************************
 * 
 * Register a service if the command type is register
 * Unregister a service if the command type is unregister
 * 
 * *****************************************************/
void Registry::HandleRequest(std::string &msg)
{
	std::map<std::string, std::string> buffer;
	parseMsg(buffer, " ", msg);
	
	std::string strType = findElement(buffer, "type");
	if (!strType.empty())
	{
		
		if (strType == "register")
		{
			buildServiceAndRegister(buffer);
		}
		else if (strType == "unregister")
		{		
			removeService(buffer);
		}
		else
			std::cout << "Command not available: " <<  strType << std::endl;
		
	}
	else
		std::cout << "Service is not created" << std::endl;
	
	std::cout << "Operation: " <<  strType << std::endl;
	
}
