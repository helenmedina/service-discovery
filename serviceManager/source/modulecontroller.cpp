#include "modulecontroller.h"
#include "authorizationhandler.h"
#include "Sender.h"
#include "Receiver.h"
#include "TextDataService.h"
#include "ImgDataService.h"
#include "Poco/Crypto/CipherFactory.h"
#include "Poco/Crypto/CipherKey.h"
#include <iostream>
#include <memory>


ModuleController& ModuleController::getInstance()
{
    static ModuleController moduleController;

    return moduleController;
}

ModuleController::~ModuleController()
{
    
    if (threadRegister.isRunning())
    {
        threadRegister.join();
    }
}

ModuleController::ModuleController()
{
	pCipher = Poco::Crypto::CipherFactory::defaultFactory().createCipher(Poco::Crypto::CipherKey("aes256"));
}

bool ModuleController::setCredentials(std::string strUsername, std::string strPassword)
{
   return (authHandler.createUser(strUsername, strPassword) == true);
}

bool ModuleController::validateCode(std::string strCode)
{
   return (authHandler.isCodeValid(strCode) == true);
}

bool ModuleController::validateCredentials(std::string strUsername, std::string strPassword)
{
   return (authHandler.isUserValid(strUsername, strPassword) == true);
}

void ModuleController::sendImageFile(std::string pathFile, std::string username, std::string filename)
{
	Poco::Thread thread;
	Sender senderFile;
	senderFile.setPathFile(pathFile);
	senderFile.setUsername(username);
	senderFile.setFilename(filename);
	senderFile.setCrypto(pCipher);
	
	std::shared_ptr<IService> serviceObj = registryObj.getService(eServicesType::ImgService);	
	std::shared_ptr<ImgDataService> imageService = std::dynamic_pointer_cast<ImgDataService>(serviceObj);
	
	if(imageService != nullptr)
	{
		senderFile.setIPService(imageService->getIP());
		senderFile.setPortService(imageService->getPort());
		
		thread.start(senderFile);
		thread.join();
	}
	
}

void ModuleController::sendTextFile(std::string pathFile, std::string username, std::string filename)
{
	Poco::Thread thread;
	Sender senderFile;
	senderFile.setPathFile(pathFile);
	senderFile.setUsername(username);
	senderFile.setFilename(filename);
	senderFile.setCrypto(pCipher);
	
	std::shared_ptr<IService> serviceObj = registryObj.getService(eServicesType::TextService);	
	std::shared_ptr<TextDataService> textService = std::dynamic_pointer_cast<TextDataService>(serviceObj);
	
	if(textService != nullptr)
	{
		senderFile.setIPService(textService->getIP());
		senderFile.setPortService(textService->getPort());
		
		thread.start(senderFile);
		thread.join();
	}
}

void ModuleController::receiveImageFile(std::string pathFile, std::string username, std::string fileToReceive, std::string filename)
{
	Poco::Thread thread;
	Receiver receiverFile;
	receiverFile.setPathFile(pathFile);
	receiverFile.setUsername(username);
	receiverFile.setFileToReceive(fileToReceive);
	receiverFile.setFilename(filename);
	receiverFile.setCrypto(pCipher);
	
	
	std::shared_ptr<IService> serviceObj = registryObj.getService(eServicesType::ImgService);	
	std::shared_ptr<ImgDataService> imageService = std::dynamic_pointer_cast<ImgDataService>(serviceObj);
	
	if(imageService != nullptr)
	{
		receiverFile.setIPService(imageService->getIP());
		receiverFile.setPortService(imageService->getPort());
		
		thread.start(receiverFile);
		thread.join();
	}
	
}
void ModuleController::receiveTextFile(std::string pathFile, std::string username, std::string fileToReceive, std::string filename)
{
	Poco::Thread thread;
	Receiver receiverFile;
	receiverFile.setPathFile(pathFile);
	receiverFile.setUsername(username);
	receiverFile.setFileToReceive(fileToReceive);
	receiverFile.setFilename(filename);
	receiverFile.setCrypto(pCipher);
	
	
	std::shared_ptr<IService> serviceObj = registryObj.getService(eServicesType::TextService);	
	std::shared_ptr<TextDataService> textService = std::dynamic_pointer_cast<TextDataService>(serviceObj);
	
	if(textService != nullptr)
	{
		receiverFile.setIPService(textService->getIP());
		receiverFile.setPortService(textService->getPort());
		
		thread.start(receiverFile);
		thread.join();
	}
	
}

void ModuleController::startRegister()
{
    threadRegister.start(registryObj);
}
