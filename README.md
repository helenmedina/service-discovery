# README #

Design and development of an app that stores text and image files for users who are suscribe in the system. For this purpose the microservices architecture is used in combination the discovery pattern.

### What is this repository for? ###

##### Quick summary #####
Service Discovery

![alt text](images/System_components_diagram.jpg)

### How do I get set up? ###

##### Configuration #####
- The dependencies folder should contains the header and binary files of Poco library. Also, this folder contains the poco.cmake to detect the poco libraries

- The microserviceStoreImg folder contains the code for the application server to store image files.

- The microserviceStoreText folder contains the code for the application server to store text files.

- The serviceManager folder contains the main app

- The config file "config.cfg" configure the parameters of the servers

- The build folder contains the binary that is generated with cmake

##### Dependencies #####
Poco

##### Deployment instructions #####
cmake -G "Unix Makefiles" ..

run make